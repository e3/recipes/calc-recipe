# calc conda recipe

Home: https://github.com/epics-modules/calc

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: APS BCDA synApps module: calc
